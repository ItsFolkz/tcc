﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Filtros;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Infra.Ordenacao.Interface;
using Infra.Ordenacao.model;
using Infra.Paginacao;
using Infra.Paginacao.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromocaoController : ControllerBase
    {
        private readonly IOrdenacao<PromocaoOut> ordenacao;
        private readonly IPromocaoService service;

        public PromocaoController(IOrdenacao<PromocaoOut> _ordenacao, IPromocaoService _service)
        {
            service = _service;
            ordenacao = _ordenacao;
        }


        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet]
        public IActionResult GetAll([FromQuery] PromocaoFiltros filtros, [FromQuery] Ordem ordem, [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = service.GetAll().AplicaFiltroPromocao(filtros);
            lista = ordenacao.AplicaOrdenacao(lista, ordem);
            var listaPaginada = Paginacao<PromocaoOut>.From(paginacao, lista);
            if (listaPaginada.ListaItens.Count > 0)
                return Ok(listaPaginada);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet("sempaginacao")]
        public IActionResult GetAllSemPaginacao([FromQuery] PromocaoFiltros filtros, [FromQuery] Ordem ordem)
        {
            var lista = service.GetAll().AplicaFiltroPromocao(filtros);
            lista = ordenacao.AplicaOrdenacao(lista, ordem);
            if (lista.Count() > 0)
                return Ok(lista);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet("{Id}")]
        public IActionResult GetById([FromRoute] long Id)
        {
            var promocao = service.Get(Id);
            if (promocao != null)
                return Ok(promocao);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPost]
        public IActionResult CreatePromocao([FromBody] PromocaoIn In)
        {
            if (ModelState.IsValid)
            {
                service.Add(In);
                return CreatedAtAction(nameof(GetById), new { Id = In.Id }, In);
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPut]
        public IActionResult UpdatePromocao([FromBody] PromocaoIn In)
        {
            if (ModelState.IsValid)
            {
                service.Update(In);
                return NoContent();
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpDelete("{Id}")]
        public IActionResult DeletePromocao([FromRoute] long Id)
        {
            service.Delete(Id);
            return NotFound();
        }
    }
}
