﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Filtros;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Infra.Ordenacao.Interface;
using Infra.Ordenacao.model;
using Infra.Paginacao.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        public readonly IProdutoService _service;
        public readonly IOrdenacao<ProdutoOut> _ordenacao;

        public ProdutoController(IProdutoService service)
        {
            _service = service;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet]
        public IActionResult GetAll([FromQuery] Ordem ordem, [FromQuery] ObjetoPaginacao paginacao, [FromQuery] ProdutoFiltros filtro)
        {
            var lista = _service.TodosProdutos().AplicaFiltroProduto(filtro);
            var lista2 = _ordenacao.AplicaOrdenacao(lista, ordem).ToList();

            if (lista2 != null || lista2.Count > 0)
                return Ok(lista);
            return NotFound();
        }
        
        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet("sempaginacao")]
        public IActionResult GetAllSemPaginacao([FromQuery] Ordem ordem, [FromQuery] ProdutoFiltros filtros)
        {
            var lista = _service.TodosProdutos().AplicaFiltroProduto(filtros);
            var lista2 = _ordenacao.AplicaOrdenacao(lista, ordem).ToList();

            if (lista2 != null || lista2.Count > 0)
                return Ok(lista2);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet("Id")]
        public IActionResult GetById([FromRoute] long Id)
        {
            var produto = _service.Get(Id);
            if (produto != null)
                return Ok(produto);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPost]
        public IActionResult Add([FromBody] ProdutoIn In)
        {
            if (ModelState.IsValid)
            {
                _service.Add(In);
                return CreatedAtAction(nameof(GetById), new { id = In.Id }, In);
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPut]
        public IActionResult Update([FromBody] ProdutoIn In)
        {
            if (ModelState.IsValid)
            {
                _service.Update(In);
                return NoContent();
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpDelete("Id")]
        public IActionResult Delete([FromRoute] long Id)
        {
            var resposta = _service.Delete(Id);
            if (resposta)
                return NoContent();
            else
                return BadRequest();
        }
    }
}