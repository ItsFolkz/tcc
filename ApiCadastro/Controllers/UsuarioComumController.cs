﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Filtros;
using Infra.Models.Database;
using Infra.Ordenacao;
using Infra.Ordenacao.model;
using Infra.Paginacao;
using Infra.Paginacao.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioComumController : ControllerBase
    {
        private readonly IUsuarioComumService usuarioComumService;
        public UsuarioComumController(IUsuarioComumService _usuarioComumService)
        {
            usuarioComumService = _usuarioComumService;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet]
        public IActionResult GetAll([FromQuery] UsuarioComumFiltros filtro, [FromQuery] Ordem ordem, [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = usuarioComumService.TodosUsuariosNormais().AplicaFiltroUsuarioNormal(filtro).AplicaOrdenacaoUsuarioNormal(ordem);
            var listaPaginada = Paginacao<UsuarioComum>.From(paginacao, lista);
            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet("SemPaginacao")]
        public IActionResult GetAllSemPaginacao([FromQuery] UsuarioComumFiltros filtro,
                                                [FromQuery] Ordem ordem)
        {
            var lista = usuarioComumService.TodosUsuariosNormais().AplicaFiltroUsuarioNormal(filtro).AplicaOrdenacaoUsuarioNormal(ordem);
            if (lista.Count() == 0) return NoContent();

            return Ok(lista);
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        [HttpGet("{Id}")]
        public IActionResult GetById([FromRoute] string Id)
        {
            var user = usuarioComumService.GetUsuario(Id);
            if (user != null)
                return Ok(user);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPut]
        public IActionResult UpdateUsuarioNormal([FromBody] UsuarioComum user)
        {
            if (ModelState.IsValid)
            {
                usuarioComumService.UpdateUsuarioNormal(user);
                return Ok();
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente")]
        [HttpDelete("{userId}")]
        public IActionResult RemoveUsuarioNormal([FromRoute] string userId)
        {
            if (!String.IsNullOrWhiteSpace(userId))
            {
                usuarioComumService.RemoveUsuarioNormal(userId);
                return NoContent();
            }
            return BadRequest();
        }
    }
}
