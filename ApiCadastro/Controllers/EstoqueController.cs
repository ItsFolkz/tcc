﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Filtros;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Infra.Ordenacao.Interface;
using Infra.Ordenacao.model;
using Infra.Paginacao;
using Infra.Paginacao.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstoqueController : ControllerBase
    {
        private readonly IEstoqueService service;
        private readonly IOrdenacao<EstoqueOut> ordenacao;
        public EstoqueController(IEstoqueService _service, IOrdenacao<EstoqueOut> _ordenacao)
        {
            service = _service;
            ordenacao = _ordenacao;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet]
        public IActionResult GetAll([FromQuery] Ordem ordem, [FromQuery] ObjetoPaginacao paginacao, [FromQuery] EstoqueFiltros filtro)
        {
            var lista = service.GetAll().AsQueryable().AplicaFiltroEstoque(filtro);
            lista = ordenacao.AplicaOrdenacao(lista, ordem);
            var listaPaginada = Paginacao<EstoqueOut>.From(paginacao, lista);
            if (listaPaginada.ListaItens.Count > 0)
                return Ok(listaPaginada);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet("sempaginacao")]
        public IActionResult GetAllSemPaginacao([FromQuery] Ordem ordem, [FromQuery] EstoqueFiltros filtro)
        {
            var lista = service.GetAll().AsQueryable().AplicaFiltroEstoque(filtro);
            lista = ordenacao.AplicaOrdenacao(lista, ordem);
            if (lista.Count() > 0)
                return Ok(lista);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet("{Id}")]
        public IActionResult GetById([FromRoute] long Id)
        {
            var model = service.Get(Id);
            if (model != null)
                return Ok(model);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPost]
        public IActionResult CreateEstoque([FromBody] EstoqueIn In)
        {
            if (ModelState.IsValid)
            {
                service.Add(In);
                return CreatedAtAction(nameof(GetById), new { Id = In.Id }, In);
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPut]
        public IActionResult UpdateEstoque([FromBody] EstoqueIn In)
        {
            if (ModelState.IsValid)
            {
                service.Update(In);
                return NoContent();
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpDelete("{Id}")]
        public IActionResult DeleteEstoque([FromRoute] long Id)
        {
            if (service.Delete(Id))
                return NotFound();
            else
                return BadRequest();
        }
    }
}
