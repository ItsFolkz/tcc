﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Models.ViewModel.In;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthService authService;
        public AuthenticationController(IAuthService _authService)
        {
            authService = _authService;
        }

        [HttpPost("AuthLogin")]
        public IActionResult AuthLogin([FromBody] LoginIn login)
        {
            if (ModelState.IsValid)
            {
                var token = authService.Authenticate(login).Result;
                if (token != null)
                    return Ok(token);
            }
            return BadRequest();
        }

        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegisterIn register)
        {
            if (ModelState.IsValid)
            {
                var resultado = authService.Register(register).Result;
                if (resultado)
                    return Ok(resultado);
            }
            return BadRequest();
        }

        [HttpPost("Refresh")]
        public IActionResult Refresh([FromBody] string RefreshToken)
        {
            if (!String.IsNullOrWhiteSpace(RefreshToken))
            {
                var refreshToken = authService.RefreshToken(RefreshToken);
                if (refreshToken != null)
                    return Ok(refreshToken);
            }
            return BadRequest();
        }
    }
}
