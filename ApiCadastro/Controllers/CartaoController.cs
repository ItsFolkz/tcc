﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Models.ViewModel.In;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartaoController : ControllerBase
    {
        private readonly ICartaoService service;
        public CartaoController(ICartaoService _service)
        {
            service = _service;
        }

        [Authorize(Roles = "Cliente")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var lista = service.GetAll();
            if (lista.Count() > 0)
                return Ok(lista);
            return NotFound();
        }

        [Authorize(Roles = "Cliente")]
        [HttpGet("{Id}")]
        public IActionResult GetById([FromRoute] long Id)
        {
            var cartao = service.Get(Id);
            if (cartao != null)
                return Ok(cartao);
            return NotFound();
        }

        [Authorize(Roles = "Cliente")]
        [HttpPost]
        public IActionResult CreateCartao([FromBody] CartaoIn In)
        {
            service.Add(In);
            return CreatedAtAction(nameof(GetById), new { Id = In.Id }, In);
        }

        [Authorize(Roles = "Cliente")]
        [HttpPut]
        public IActionResult UpdateCartao([FromBody] CartaoIn In)
        {
            service.Update(In);
            return NoContent();
        }

        [Authorize(Roles = "Cliente")]
        [HttpDelete]
        public IActionResult DeleteCartao([FromRoute] long Id)
        {
            service.Delete(Id);
            return NotFound();
        }
    }
}
