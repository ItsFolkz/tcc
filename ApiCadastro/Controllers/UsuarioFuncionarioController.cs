﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Filtros;
using Infra.Models.Database;
using Infra.Ordenacao;
using Infra.Ordenacao.model;
using Infra.Paginacao;
using Infra.Paginacao.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioFuncionarioController : ControllerBase
    {
        private readonly IUsuarioFuncionarioService usuarioFuncionarioService;
        public UsuarioFuncionarioController(IUsuarioFuncionarioService _usuarioFuncionarioService)
        {
            usuarioFuncionarioService = _usuarioFuncionarioService;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet]
        public IActionResult GetAllPaginado([FromQuery] UsuarioFuncionarioFiltros filtro,
                                    [FromQuery] Ordem ordem,
                                    [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = usuarioFuncionarioService.TodosFuncionarios
                                                    .AplicaFiltroFuncionario(filtro)
                                                    .AplicaOrdenacaoFuncionario(ordem);
            var listaPaginada = Paginacao<UsuarioFuncionario>.From(paginacao, lista);
            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet("SemPaginacao")]
        public IActionResult GetAllSemPaginacao([FromQuery] UsuarioFuncionarioFiltros filtro,
                                                [FromQuery] Ordem ordem)
        {
            var lista = usuarioFuncionarioService.TodosFuncionarios
                                                    .AplicaFiltroFuncionario(filtro).AplicaOrdenacaoFuncionario(ordem);
            if (lista.Count() == 0) return NoContent();

            return Ok(lista);
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpGet("{Id}")]
        public IActionResult GetUsuario([FromRoute] string Id)
        {
            var user = usuarioFuncionarioService.GetUsuario(Id);
            if (user != null)
                return Ok(user);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpPut]
        public IActionResult UpdateFuncionario([FromBody] UsuarioFuncionario funcionario)
        {
            if (ModelState.IsValid)
            {
                usuarioFuncionarioService.UpdateFuncionario(funcionario);
                return Ok();
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        [HttpDelete("{userId}")]
        public IActionResult RemoveFuncionario([FromRoute] string userId)
        {
            if (!String.IsNullOrWhiteSpace(userId))
            {
                usuarioFuncionarioService.RemoveFuncionario(userId);
                return NoContent();
            }
            return BadRequest();
        }
    }
}
