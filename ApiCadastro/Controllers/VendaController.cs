﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Service.Interface;
using Infra.Filtros;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Infra.Ordenacao.Interface;
using Infra.Ordenacao.model;
using Infra.Paginacao;
using Infra.Paginacao.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCadastro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly IOrdenacao<VendaOut> ordenacao;
        private readonly IVendaService service;

        public VendaController(IOrdenacao<VendaOut> _ordenacao, IVendaService _service)
        {
            ordenacao = _ordenacao;
            service = _service;
        }

        [Authorize(Roles = "Administrador, Gerente, Cliente")]
        [HttpGet]
        public IActionResult GetAll([FromQuery] VendaFiltros filtro, [FromQuery] Ordem ordem, [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = service.GetAll().AplicaFiltroVenda(filtro);
            lista = ordenacao.AplicaOrdenacao(lista, ordem);
            var listaPaginada = Paginacao<VendaOut>.From(paginacao, lista);
            if (listaPaginada.ListaItens.Count > 0)
                return Ok(listaPaginada);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Cliente")]
        [HttpGet("sempaginacao")]
        public IActionResult GetAllSemPaginacao([FromQuery] VendaFiltros filtro, [FromQuery] Ordem ordem)
        {
            var lista = service.GetAll().AplicaFiltroVenda(filtro);
            lista = ordenacao.AplicaOrdenacao(lista, ordem);
            if (lista.Count() > 0)
                return Ok(lista);
            return NotFound();
        }

        [Authorize(Roles = "Administrador, Gerente, Cliente")]
        [HttpGet("{Id}")]
        public IActionResult GetById([FromRoute] long Id)
        {
            var venda = service.Get(Id);
            if (venda != null)
                return Ok(venda);
            return NotFound();
        }

        [HttpPost]
        public IActionResult CreateVenda([FromBody] VendaIn In)
        {
            if (ModelState.IsValid)
            {
                service.Add(In);
                return CreatedAtAction(nameof(GetById), new { Id = In.Id }, In);
            }
            return BadRequest();
        }

        [Authorize(Roles = "Cliente")]
        [HttpPost("efetuarcompra")]
        public IActionResult EfetuarCompra([FromBody] VendaIn In)
        {
            if (ModelState.IsValid)
            {
                var response = service.EfetuarCompra(In);
                return Ok(response);
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Cliente")]
        [HttpPut]
        public IActionResult UpdateVenda([FromBody] VendaIn In)
        {
            if (ModelState.IsValid)
            {
                service.Update(In);
                return NoContent();
            }
            return BadRequest();
        }

        [Authorize(Roles = "Administrador, Gerente, Cliente")]
        [HttpDelete("{Id}")]
        public IActionResult DeleteVenda([FromRoute] long Id)
        {
            service.Delete(Id);
            return NotFound();
        }
    }
}
