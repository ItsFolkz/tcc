using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCadastro.Data;
using Infra.Models.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using System.Text;

namespace ApiCadastro
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.AllowAnyHeader()
                            .AllowAnyOrigin()
                            .AllowAnyMethod();
                });
            });

            #region Json Configuration (Loop Handling and Null Handling)

            #endregion

            #region Swagger
            services.AddSwaggerGen(sw =>
            {
                sw.SwaggerDoc("SuperMarket Delivery", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "ApiCadastro",
                    Description = "Designed to Teste the API below."
                });
            });
            #endregion

            #region Services-Repository
            #region Services
            #endregion
            #region Repository

            #endregion
            #endregion

            #region JWT
            string redisConnection = Configuration["ConnectionStrings:redisDb"];
            services.AddDistributedRedisCache(options => 
                {
                    options.Configuration = redisConnection;
                    options.InstanceName = "RefreshToken";
                });
            services.AddIdentity<UsuarioBase, IdentityRole>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<Context>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Autentication:SecurityKey"])),
                        ValidIssuer = Configuration["Autentication:Issuer"],
                        ValidAudience = Configuration["Autentication:Audience"]
                    };
                });
            #endregion

            #region Connection to DB
            string connection = Configuration["ConnectionStrings:SuperMakertDeliveryDB"];
            services.AddDbContext<Context>(options => options.UseSqlServer(connection), ServiceLifetime.Transient);
            //services.AddDbContext<Context>(options => options.UseNpgsql(connection));

            #endregion

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
