﻿using Infra.Models.Database;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ApiCadastro.Data
{
    public class Context : IdentityDbContext<UsuarioBase>
    {
        public Context(DbContextOptions options) : base(options) { }

        public DbSet<Cartao> Cartao { get; set; }
        public DbSet<Contato> Contato { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Estabelecimento> Estabelecimento { get; set; }
        public DbSet<Estoque> Estoque { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Promocao> Promocao { get; set; }
        public DbSet<ShopCart> ShopCart { get; set; }
        public DbSet<ShopItem> ShopItem { get; set; }
        public DbSet<Venda> Venda { get; set; }
        public DbSet<UsuarioComum> Cliente { get; set; }
        public DbSet<UsuarioFuncionario> Funcionario { get; set; }
        public DbSet<Perfil> Perfil { get; set; }
    }
}
