﻿using ApiCadastro.Data;
using ApiCadastro.Repository.Interface;
using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Repository
{
    public class EstoqueRepository : Repository<Estoque>, IEstoqueRepository
    {
        private readonly Context context;

        public EstoqueRepository(Context _context) : base(_context)
        {
            context = _context;
        }
    }
}
