﻿using ApiCadastro.Data;
using ApiCadastro.Repository.Interface;
using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Repository
{
    public class UsuarioComumRepository : Repository<UsuarioComum>, IUsuarioComumRepository
    {
        private readonly Context context;
        public UsuarioComumRepository(Context _context) : base(_context)
        {

        }
    }
}
