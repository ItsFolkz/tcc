﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Repository.Interface
{
    public interface IPromocaoRepository : IRepository<Promocao>
    {
    }
}
