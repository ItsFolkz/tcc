﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Repository.Interface
{
    public interface IRepository<T> where T : class
    {
        T Get(long id);
        IQueryable<T> GetAll();
        void Add(T model);
        void Update(T model);
        void Remove(T model);
    }
}
