﻿using ApiCadastro.Data;
using ApiCadastro.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly Context _db;
        internal DbSet<T> dbSet;

        public Repository(Context db)
        {
            _db = db;
            dbSet = _db.Set<T>();
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            dbSet.Update(entity);
        }

        public T Get(long id)
        {
            return dbSet.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            var query = dbSet.AsQueryable();
            return query;
        }

        public void Remove(T entity)
        {
            dbSet.Remove(entity);
        }
    }
}
