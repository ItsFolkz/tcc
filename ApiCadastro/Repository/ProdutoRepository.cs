﻿using ApiCadastro.Data;
using ApiCadastro.Repository.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Repository
{
    public class ProdutoRepository : Repository<Produto>, IProdutoRepository
    {
        private readonly Context context;

        public ProdutoRepository(Context _context) : base(_context)
        {
            context = _context;
        }
    }
}
