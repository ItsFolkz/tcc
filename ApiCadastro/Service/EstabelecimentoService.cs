﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class EstabelecimentoService : IEstabelecimentoService
    {
        private readonly IEstabelecimentoRepository repository;
        public EstabelecimentoService(IEstabelecimentoRepository _repository)
        {
            repository = _repository;
        }

        public void Add(EstabelecimentoIn In)
        {
            var model = EstabelecimentoExtensions.ToModel(In);
            repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = repository.Get(Id);
            repository.Remove(model);
            return true;
        }

        public EstabelecimentoOut Get(long id)
        {
            var model = repository.Get(id);
            var modelOut = EstabelecimentoExtensions.ToOut(model);
            return modelOut;
        }

        public IQueryable<EstabelecimentoOut> GetAll()
        {
            var lista = repository.GetAll().ToList();
            List<EstabelecimentoOut> listaOut = new List<EstabelecimentoOut>();
            foreach (var item in lista)
            {
                var modelOut = EstabelecimentoExtensions.ToOut(item);
                listaOut.Add(modelOut);
            }
            if (listaOut.Count > 0)
                return listaOut.AsQueryable();
            return null;
        }

        public void Update(EstabelecimentoIn In)
        {
            var model = repository.GetAll()
                                    .Include(x => x.Endereco)
                                    .Include(x => x.Responsavel).Where(x => x.Id == In.Id).SingleOrDefault();
            if (model != null)
            {
                if (In.Endereco != null)
                {
                    if (model.Endereco.Bairro != In.Endereco.Bairro) model.Endereco.Bairro = In.Endereco.Bairro;
                    if (model.Endereco.Cep != In.Endereco.Cep) model.Endereco.Cep = In.Endereco.Cep;
                    if (model.Endereco.Cidade != In.Endereco.Cidade) model.Endereco.Cidade = In.Endereco.Cidade;
                    if (model.Endereco.Complemento != In.Endereco.Complemento) model.Endereco.Complemento = In.Endereco.Complemento;
                    if (model.Endereco.Estado != In.Endereco.Estado) model.Endereco.Estado = In.Endereco.Estado;
                    if (model.Endereco.Logradouro != In.Endereco.Logradouro) model.Endereco.Logradouro = In.Endereco.Logradouro;
                    if (model.Endereco.Numero != In.Endereco.Numero) model.Endereco.Numero = In.Endereco.Numero;
                    model.Endereco.DataModificacao = DateTimeOffset.UtcNow;
                }
                if (In.Responsavel != null)
                {
                    if (model.Responsavel.NomeCompleto != In.Responsavel.NomeCompleto) model.Responsavel.NomeCompleto = In.Responsavel.NomeCompleto;
                    if (model.Responsavel.Telefone != In.Responsavel.Telefone) model.Responsavel.Telefone = In.Responsavel.Telefone;
                    if (model.Responsavel.Cargo != In.Responsavel.Cargo) model.Responsavel.Cargo = In.Responsavel.Cargo;
                    if (model.Responsavel.Celular != In.Responsavel.Celular) model.Responsavel.Celular = In.Responsavel.Celular;
                    model.Responsavel.DataModificacao = DateTimeOffset.UtcNow;
                }
                if (model.DocumentoIdentificacao != In.DocumentoIdentificacao) model.DocumentoIdentificacao = In.DocumentoIdentificacao;
                if (model.Nome != In.Nome) model.Nome = In.Nome;
                if (model.RazaoSocial != In.RazaoSocial) model.RazaoSocial = In.RazaoSocial;
                model.DataModificacao = DateTimeOffset.UtcNow;
            }
            repository.Update(model);
        }
    }
}
