﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class UsuarioComumService : IUsuarioComumService
    {
        private readonly IUsuarioComumRepository usuarioComumRepository;
        public UsuarioComumService(IUsuarioComumRepository _usuarioComumRepository)
        {
            usuarioComumRepository = _usuarioComumRepository;
        }

        public IQueryable<UsuarioComum> TodosUsuariosNormais() 
        {
            var query = usuarioComumRepository.GetAll();
            query = query.Include(x => x.Endereco).Include(x => x.Cartoes);
            return query;
        }

        public UsuarioComum GetUsuario(string Id)
        {
            var query = usuarioComumRepository.GetAll();
            var user = query.Include(x => x.Endereco).Include(x => x.Cartoes).Where(x => x.Id.Equals(Id)).SingleOrDefault();
            return user;
        }

        public void RemoveUsuarioNormal(string userId)
        {
            var userDb = usuarioComumRepository.GetAll()
                                                    .Include(x => x.Perfil)
                                                    .Where(x => x.Id.Equals(userId))
                                                    .SingleOrDefault();
            usuarioComumRepository.Remove(userDb);
        }

        public void UpdateUsuarioNormal(UsuarioComum usuario)
        {
            var userDb = usuarioComumRepository.GetAll()
                                                    .Include(x => x.Perfil)
                                                    .Include(x => x.Endereco)
                                                    .Where(x => x.Id.Equals(usuario.Id))
                                                    .SingleOrDefault();
            if (usuario.NomeCompleto != userDb.NomeCompleto) userDb.NomeCompleto = usuario.NomeCompleto;
            if (usuario.Telefone != userDb.Telefone) userDb.Telefone = usuario.Telefone;
            if (usuario.PhoneNumber != userDb.Telefone) userDb.PhoneNumber = usuario.PhoneNumber;
            if (usuario.Endereco != null)
            {
                if (usuario.Endereco.Logradouro != userDb.Endereco.Logradouro) userDb.Endereco.Logradouro = usuario.Endereco.Logradouro;
                if (usuario.Endereco.Numero != userDb.Endereco.Numero) userDb.Endereco.Numero = usuario.Endereco.Numero;
                if (usuario.Endereco.Estado != userDb.Endereco.Estado) userDb.Endereco.Estado = usuario.Endereco.Estado;
                if (usuario.Endereco.Complemento != userDb.Endereco.Complemento) userDb.Endereco.Complemento = usuario.Endereco.Complemento;
                if (usuario.Endereco.Cidade != userDb.Endereco.Cidade) userDb.Endereco.Cidade = usuario.Endereco.Cidade;
                if (usuario.Endereco.Cep != userDb.Endereco.Cep) userDb.Endereco.Cep = usuario.Endereco.Cep;
                if (usuario.Endereco.Bairro != userDb.Endereco.Bairro) userDb.Endereco.Bairro = usuario.Endereco.Bairro;

                userDb.Endereco.DataModificacao = DateTimeOffset.UtcNow;
            }
            usuarioComumRepository.Update(userDb);
        }
    }
}
