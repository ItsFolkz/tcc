﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class ContatoService : IContatoService
    {
        private readonly IContatoRepository _repository;
        public ContatoService(IContatoRepository repository)
        {
            _repository = repository;
        }

        public void Add(ContatoIn In)
        {
            var model = ContatoExtensions.ToModel(In);
            model.DataCriacao = DateTimeOffset.UtcNow;
            _repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = _repository.GetAll().Where(x => x.Id == Id).SingleOrDefault();
            if (model == null)
                return false;

            _repository.Remove(model);
            return true;
        }

        public Contato Get(long id)
        {
            var model = _repository.GetAll().Where(x => x.Id == id).SingleOrDefault();
            if (model == null)
                return null;
            return model;
        }

        public IList<ContatoOut> GetAll()
        {
            var listaContatos = _repository.GetAll().ToList();
            List<ContatoOut> Contatos = new List<ContatoOut>();
            foreach (var item in listaContatos)
            {
                var endereco = ContatoExtensions.ToOut(item);
                Contatos.Add(endereco);
            }

            if (Contatos.Count > 0)
                return Contatos;

            return null;
        }

        public void Update(ContatoIn In)
        {
            var model = _repository.GetAll().Include(x => x.Endereco).Where(x => x.Id == In.Id).SingleOrDefault();

            if (model != null)
            {
                if (model.NomeCompleto != In.NomeCompleto) model.NomeCompleto = In.NomeCompleto;
                if (model.Telefone != In.Telefone) model.Telefone = In.Telefone;
                if (model.Cargo != In.Cargo) model.Cargo = In.Cargo;
                if (model.Celular != In.Celular) model.Celular = In.Celular;
                if (In.Endereco != null)
                {
                    if (model.Endereco.Bairro != In.Endereco.Bairro) model.Endereco.Bairro = In.Endereco.Bairro;
                    if (model.Endereco.Cep != In.Endereco.Cep) model.Endereco.Cep = In.Endereco.Cep;
                    if (model.Endereco.Cidade != In.Endereco.Cidade) model.Endereco.Cidade = In.Endereco.Cidade;
                    if (model.Endereco.Complemento != In.Endereco.Complemento) model.Endereco.Complemento = In.Endereco.Complemento;
                    if (model.Endereco.Estado != In.Endereco.Estado) model.Endereco.Estado = In.Endereco.Estado;
                    if (model.Endereco.Logradouro != In.Endereco.Logradouro) model.Endereco.Logradouro = In.Endereco.Logradouro;
                    if (model.Endereco.Numero != In.Endereco.Numero) model.Endereco.Numero = In.Endereco.Numero;
                }
                model.DataModificacao = DateTimeOffset.UtcNow;

                _repository.Update(model);
            }
        }
    }
}
