﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class UsuarioFuncionarioService : IUsuarioFuncionarioService
    {
        private readonly IUsuarioFuncionarioRepository usuarioFuncionarioRepository;
        public UsuarioFuncionarioService(IUsuarioFuncionarioRepository _usuarioFuncionarioRepository)
        {
            usuarioFuncionarioRepository = _usuarioFuncionarioRepository;
        }

        public IQueryable<UsuarioFuncionario> TodosFuncionarios => usuarioFuncionarioRepository.GetAll();

        public UsuarioFuncionario GetUsuario(string Id)
        {
            var query = usuarioFuncionarioRepository.GetAll();
            var user = query.Include(x => x.Estabelecimento).Where(x => x.Id.Equals(Id)).SingleOrDefault();
            return user;
        }

        public void UpdateFuncionario(UsuarioFuncionario usuario)
        {
            var funcionarioDB = usuarioFuncionarioRepository.GetAll()
                                                                .Include(x => x.Perfil)
                                                                .Where(x => x.Id.Equals(usuario.Id))
                                                                .SingleOrDefault();
            if (usuario.NomeCompleto != funcionarioDB.NomeCompleto) funcionarioDB.NomeCompleto = usuario.NomeCompleto;
            if (usuario.IdFuncionario != funcionarioDB.IdFuncionario) funcionarioDB.IdFuncionario = usuario.IdFuncionario;
            if (usuario.Telefone != funcionarioDB.Telefone) funcionarioDB.Telefone = usuario.Telefone;
            if (usuario.PhoneNumber != funcionarioDB.Telefone) funcionarioDB.PhoneNumber = usuario.PhoneNumber;
            if (usuario.Cargo != usuario.Cargo) funcionarioDB.Cargo = usuario.Cargo;

            usuarioFuncionarioRepository.Update(funcionarioDB);
        }

        public void RemoveFuncionario(string userId)
        {
            var funcDb = usuarioFuncionarioRepository.GetAll()
                                                        .Include(x => x.Perfil)
                                                        .Where(x => x.Id.Equals(userId))
                                                        .SingleOrDefault();
            usuarioFuncionarioRepository.Remove(funcDb);
        }
    }
}
