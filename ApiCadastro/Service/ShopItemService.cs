﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class ShopItemService : IShopItemService
    {
        private readonly IShopItemRepository repository;
        public ShopItemService(IShopItemRepository _repository)
        {
            repository = _repository;
        }

        public void Add(ShopItemIn In)
        {
            var model = ShopItemExtensions.ToModel(In);
            repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = repository.Get(Id);
            repository.Remove(model);
            return true;
        }

        public ShopItemOut Get(long id)
        {
            var model = repository.GetAll().Include(x => x.Produto).Where(x => x.Id.Equals(id)).SingleOrDefault();
            var modelOut = ShopItemExtensions.ToOut(model);
            return modelOut;
        }

        public IQueryable<ShopItemOut> GetAll()
        {
            var lista = repository.GetAll();
            lista = lista.Include(x => x.Produto);
            List<ShopItemOut> listaOut = new List<ShopItemOut>();
            foreach (var item in lista)
            {
                var modelOut = ShopItemExtensions.ToOut(item);
                listaOut.Add(modelOut);
            }
            return listaOut.AsQueryable();
        }

        public void Update(ShopItemIn In)
        {
            throw new NotImplementedException();
        }
    }
}
