﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepository _repository;

        public ProdutoService(IProdutoRepository repository)
        {
            _repository = repository;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        public IQueryable<ProdutoOut> TodosProdutos()
        {
            var lista = _repository.GetAll().ToList();
            List<ProdutoOut> listaProdutos = new List<ProdutoOut>();

            foreach (var produto in lista)
            {
                var prod = ProdutoExtensions.ToOut(produto);
                listaProdutos.Add(prod);
            }
            if (listaProdutos.Count > 0)
                return listaProdutos.AsQueryable();
            return null;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista, Cliente")]
        public Produto Get(long id)
        {
            var produto = _repository.GetAll().Where(x => x.Id == id).SingleOrDefault();
            if (produto != null)
                return produto;
            return null;
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        public void Add(ProdutoIn In)
        {
            var model = ProdutoExtensions.ToModel(In);
            model.DataCriacao = DateTimeOffset.UtcNow;
            _repository.Add(model);
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        public void Update(ProdutoIn In)
        {
            var model = _repository.GetAll().Where(x => x.Id == In.Id).SingleOrDefault();

            if (model.Descricao != In.Descricao) model.Descricao = In.Descricao;
            if (model.Lote != In.Lote) model.Lote = In.Lote;
            if (model.Marca != In.Marca) model.Marca = In.Marca;
            if (model.Nome != In.Nome) model.Nome = In.Nome;
            if (model.Tipo != In.Tipo) model.Tipo = In.Tipo;
            if (model.Validade != In.Validade) model.Validade = In.Validade;
            if (model.Valor != In.Valor) model.Valor = In.Valor;
            if (model.Codigo != In.Codigo) model.Codigo = In.Codigo;

            _repository.Update(model);
        }

        [Authorize(Roles = "Administrador, Gerente, Estoquista")]
        public bool Delete(long Id)
        {
            var model = _repository.GetAll().Where(x => x.Id == Id).SingleOrDefault();
            if (model == null)
                return false;

            _repository.Remove(model);
            return true;
        }
    }
}
