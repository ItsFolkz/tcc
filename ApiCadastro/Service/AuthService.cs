﻿using ApiCadastro.Repository.Interface;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Infra.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Infra.Models.RedisModel;
using Newtonsoft.Json;
using ApiCadastro.Service.Interface;

namespace ApiCadastro.Service
{
    public class AuthService : IAuthService
    {
        private readonly IConfiguration _configuration;
        private readonly IUsuarioComumRepository _usuarioComumRepository;
        private readonly IUsuarioFuncionarioRepository _usuarioFuncionarioRepository;
        private readonly IDistributedCache _distributedCache;
        private readonly SignInManager<UsuarioBase> _signInManager;
        private readonly UserManager<UsuarioBase> _userManager;
        public AuthService(IConfiguration configuration, IUsuarioComumRepository usuarioComumRepository,
            IUsuarioFuncionarioRepository usuarioFuncionarioRepository,
            IDistributedCache distributedCache,
            SignInManager<UsuarioBase> signInManager,
            UserManager<UsuarioBase> userManager)
        {
            _configuration = configuration;
            _usuarioComumRepository = usuarioComumRepository;
            _usuarioFuncionarioRepository = usuarioFuncionarioRepository;
            _distributedCache = distributedCache;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<AuthOut> Authenticate(LoginIn login)
        {
            var tokenOut = new AuthOut();
            if (login.Tipo == TipoLoginEnum.Funcionario || login.Tipo == TipoLoginEnum.Admin)
            {
                var resultado = await _signInManager.PasswordSignInAsync(login.Username, login.Senha, true, true);
                if (resultado.Succeeded)
                {
                    var query = _usuarioFuncionarioRepository.GetAll();
                    query = query.Include(x => x.Estabelecimento).Include(x => x.Perfil);
                    var usuario = query.Where(x => x.UserName.Equals(login.Username)).SingleOrDefault();

                    JwtSecurityToken token = CreateToken(null, usuario, login.Tipo);
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                    var refreshToken = new RefreshToken
                    {
                        UserId = usuario.Id,
                        UserName = usuario.UserName,
                        Tipo = login.Tipo,
                        Refresh = Guid.NewGuid().ToString(),
                        Expiration = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Autentication:RefreshTokenExpiryInMinutes"]))
                    };
                    _distributedCache.SetString(refreshToken.Refresh, JsonConvert.SerializeObject(refreshToken));

                    var auth = new AuthOut()
                    {
                        TokenString = tokenString,
                        RefreshToken = refreshToken.Refresh,
                        TokenExpiration = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Autentication:ExpiryInMinutes"]))
                    };
                    tokenOut = auth;
                }
            }
            else if(login.Tipo == TipoLoginEnum.Normal)
            {
                var resultado = await _signInManager.PasswordSignInAsync(login.Username, login.Senha, true, true);
                if (resultado.Succeeded)
                {
                    var query = _usuarioComumRepository.GetAll();
                    query = query.Include(x => x.Perfil);
                    var usuario = query.Where(x => x.UserName.Equals(login.Username)).SingleOrDefault();

                    JwtSecurityToken token = CreateToken(usuario, null, login.Tipo);
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                    var refreshToken = new RefreshToken
                    {
                        UserId = usuario.Id,
                        UserName = usuario.UserName,
                        Refresh = Guid.NewGuid().ToString(),
                        Expiration = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Autentication:RefreshTokenExpiryInMinutes"]))
                    };
                    _distributedCache.SetString(refreshToken.Refresh, JsonConvert.SerializeObject(refreshToken));

                    var tokenSaida = new AuthOut()
                    {
                        TokenString = tokenString,
                        RefreshToken = refreshToken.Refresh,
                        TokenExpiration = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Autentication:ExpiryInMinutes"]))
                    };
                    tokenOut = tokenSaida;
                }
            }
            return tokenOut;
        }

        public async Task<bool> Register(RegisterIn register)
        {
            if (register.Tipo == TipoLoginEnum.Funcionario)
            {
                var user = new UsuarioFuncionario()
                {
                    UserName = register.Funcionario.Username,
                    NomeCompleto = register.Funcionario.NomeCompleto,
                    PhoneNumber = register.Funcionario.Telefone,
                    Telefone = register.Funcionario.Telefone,
                    DocumentoIdentificacao = register.Funcionario.DocumentoIdentificacao,
                    IdFuncionario = register.Funcionario.IdFuncionario
                };
                var result = await _userManager.CreateAsync(user, register.Funcionario.Senha);
                if (result.Succeeded)
                    return true;
                else
                    return false;
            }
            else
            {
                var user = new UsuarioComum()
                {
                    UserName = register.Comum.Username,
                    NomeCompleto = register.Comum.NomeCompleto,
                    PhoneNumber = register.Comum.Telefone,
                    Telefone = register.Comum.Telefone,
                    DocumentoIdentificacao = register.Comum.DocumentoIdentificacao
                };
                var result = await _userManager.CreateAsync(user, register.Comum.Senha);
                if (result.Succeeded)
                    return true;
                else
                    return false;
            }
        }

        public AuthOut RefreshToken(string rfToken)
        {
            var obj = _distributedCache.GetString(rfToken);

            var refreshToken = JsonConvert.DeserializeObject<RefreshToken>(obj);

            if (refreshToken == null)
            {
                return null;
            }

            JwtSecurityToken token = new JwtSecurityToken();
            if (refreshToken.Tipo == TipoLoginEnum.Funcionario)
            {
                var user = _usuarioFuncionarioRepository.GetAll()
                               .Include(x => x.Perfil)
                               .Include(x => x.Estabelecimento)
                               .Where(x => x.Id.Equals(refreshToken.UserId))
                               .SingleOrDefault();
                token = CreateToken(null, user, refreshToken.Tipo);
            }
            else
            {
                var user = _usuarioComumRepository.GetAll()
                                .Include(c => c.Perfil)
                                .Where(x => x.Id.Equals(refreshToken.UserId))
                                .SingleOrDefault();
                token = CreateToken(user, null, refreshToken.Tipo);
            }

            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            var tokenRessource = new AuthOut
            {
                TokenString = tokenString,
                RefreshToken = refreshToken.Refresh,
                TokenExpiration = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Autentication:ExpiryInMinutes"]))
            };

            return tokenRessource;
        }

        private JwtSecurityToken CreateToken(UsuarioComum usuarioComum, UsuarioFuncionario usuarioFuncionario, TipoLoginEnum tipo)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Autentication:SecurityKey"]));
            int expirationTime = Convert.ToInt32(_configuration["Autentication:ExpiryInMinutes"]);
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            switch (tipo)
            {
                case TipoLoginEnum.Normal:
                    var claimsFunc = new[]
{
                        new Claim(JwtRegisteredClaimNames.Sub, usuarioFuncionario.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim("Id", usuarioFuncionario.Id.ToString()),
                        new Claim("Estabelecimento", usuarioFuncionario.Estabelecimento.Id.ToString()),
                        new Claim(ClaimTypes.Role, usuarioFuncionario.Perfil.TipoPerfil.ToString())
                    };

                    var tokenFunc = new JwtSecurityToken(
                        issuer: _configuration["Autentication:Issuer"],
                        audience: _configuration["Autentication:Audience"],
                        claims: claimsFunc,
                        signingCredentials: credentials,
                        expires: DateTime.Now.AddMinutes(expirationTime)
                        );
                    return tokenFunc;
                case TipoLoginEnum.Admin:
                case TipoLoginEnum.Funcionario:
                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, usuarioFuncionario.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim("Id", usuarioFuncionario.Id.ToString()),
                        new Claim("Estabelecimento", usuarioFuncionario.Estabelecimento.Id.ToString()),
                        new Claim(ClaimTypes.Role, usuarioFuncionario.Perfil.TipoPerfil.ToString())
                    };

                    var token = new JwtSecurityToken(
                        issuer: _configuration["Autentication:Issuer"],
                        audience: _configuration["Autentication:Audience"],
                        claims: claims,
                        signingCredentials: credentials,
                        expires: DateTime.Now.AddMinutes(expirationTime)
                        );
                    return token;
                default:
                    return null;
            }
        }
    }
}
