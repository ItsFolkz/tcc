﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class ShopCartService : IShopCartService
    {
        private readonly IShopCartRepository repository;
        public ShopCartService(IShopCartRepository _repository)
        {
            repository = _repository;
        }

        public void Add(ShopCartIn In)
        {
            var model = ShopCartExtensions.ToModel(In);
            repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = repository.Get(Id);
            repository.Remove(model);
            return true;
        }

        public ShopCartOut Get(long id)
        {
            var model = repository.GetAll().Include(x => x.Items).Where(x => x.Id.Equals(id)).SingleOrDefault();
            var modelOut = ShopCartExtensions.ToOut(model);
            return modelOut;
        }

        public IQueryable<ShopCartOut> GetAll()
        {
            var lista = repository.GetAll();
            lista = lista.Include(x => x.Items);
            List<ShopCartOut> listaOut = new List<ShopCartOut>();
            foreach (var item in lista)
            {
                var modelOut = ShopCartExtensions.ToOut(item);
                listaOut.Add(modelOut);
            }
            return listaOut.AsQueryable();
        }

        public void Update(ShopCartIn In)
        {
            throw new NotImplementedException();
        }
    }
}
