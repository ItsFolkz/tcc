﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Middleware;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using ModuloMiddleware.Service.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepository repository;
        private readonly IMiddlewareService middleware;
        public VendaService(IVendaRepository _repository, IMiddlewareService _middleware)
        {
            repository = _repository;
            middleware = _middleware;
        }

        public void Add(VendaIn In)
        {
            var model = VendaExtensions.ToModel(In);
            repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = repository.Get(Id);
            repository.Remove(model);
            return true;
        }

        public VendaOut Get(long id)
        {
            var model = repository.GetAll()
                                    .Include(x => x.Cartao)
                                    .Include(x => x.ShopCart)
                                    .Include(x => x.Usuario).Where(x => x.Id.Equals(id)).SingleOrDefault();
            var modelOut = VendaExtensions.ToOut(model);
            return modelOut;
        }

        public IQueryable<VendaOut> GetAll()
        {
            var lista = repository.GetAll();
            lista = lista.Include(x => x.Cartao).Include(x => x.ShopCart).Include(x => x.Usuario);
            List<VendaOut> listaOut = new List<VendaOut>();
            foreach (var item in lista)
            {
                var modelOut = VendaExtensions.ToOut(item);
                listaOut.Add(modelOut);
            }
            return listaOut.AsQueryable();
        }

        public void Update(VendaIn In)
        {
            throw new NotImplementedException();
        }

        public MiddlewareResponse EfetuarCompra(VendaIn In)
        {
            var response = middleware.executePayment(In);
            if (response.Autorizado)
            {
                Add(In);
                return response;
            }
            return new MiddlewareResponse()
            {
                Compra = In,
                Autorizado = false,
                RespostaAutorizacao = "Compra não foi autorizada devido a algum problema com o cartão."
            };
        }
    }
}
