﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.CodeAnalysis.Operations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class CartaoService : ICartaoService
    {
        private readonly ICartaoRepository repository;
        private readonly IUsuarioComumRepository usuarioComumRepository;
        public CartaoService(ICartaoRepository _repository, IUsuarioComumRepository userRepository)
        {
            repository = _repository;
            usuarioComumRepository = userRepository;
        }

        public void Add(CartaoIn In)
        {
            var model = CartaoExtensions.ToModel(In);
            var query = usuarioComumRepository.GetAll();
            query = query.Include(x => x.Cartoes);
            var user = query.Where(x => x.Id.Equals(In.IdUsuario)).SingleOrDefault();
            user.Cartoes.Add(model);
            repository.Add(model);
            usuarioComumRepository.Update(user);
        }

        public bool Delete(long Id)
        {
            var cartao = repository.Get(Id);
            repository.Remove(cartao);
            return true;
        }

        public CartaoOut Get(long id)
        {
            var model = repository.Get(id);
            var modelOut = CartaoExtensions.ToOut(model);
            if (modelOut != null)
                return modelOut;
            return null;
        }

        public IQueryable<CartaoOut> GetAll()
        {
            var lista = repository.GetAll().ToList();
            List<CartaoOut> listaOut = new List<CartaoOut>();
            foreach (var item in lista)
            {
                var modelOut = CartaoExtensions.ToOut(item);
                listaOut.Add(modelOut);
            }
            if (listaOut.Count > 0)
                return listaOut.AsQueryable();
            return null;
        }

        public void Update(CartaoIn In)
        {
            var model = repository.GetAll().Where(x => x.Id.Equals(In.Id)).SingleOrDefault();
            if (model != null)
            {
                if (model.CodigoSeguranca != In.CodigoSeguranca) model.CodigoSeguranca = In.CodigoSeguranca;
                if (model.CpfTitular != In.CpfTitular) model.CpfTitular = In.CpfTitular;
                if (model.DataValidade != In.DataValidade) model.DataValidade = In.DataValidade;
                if (model.NomeTitular != In.NomeTitular) model.NomeTitular = In.NomeTitular;
                if (model.NumeroCartao != In.NumeroCartao) model.NumeroCartao = In.NumeroCartao;
            }
            repository.Update(model);
        }
    }
}
