﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class PromocaoService : IPromocaoService
    {
        private readonly IPromocaoRepository repository;
        public PromocaoService(IPromocaoRepository _repository)
        {
            repository = _repository;
        }

        public void Add(PromocaoIn In)
        {

            var model = PromocaoExtensions.ToModel(In);
            repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = repository.Get(Id);
            repository.Remove(model);
            return true;
        }

        public PromocaoOut Get(long id)
        {
            var model = repository.Get(id);
            var modelOut = PromocaoExtensions.ToOut(model);
            return modelOut;
        }

        public IQueryable<PromocaoOut> GetAll()
        {
            var lista = repository.GetAll().Include(x => x.Produto).ToList();
            List<PromocaoOut> listaOut = new List<PromocaoOut>();
            foreach (var item in lista)
            {
                var modelOut = PromocaoExtensions.ToOut(item);
                listaOut.Add(modelOut);
            }
            if (listaOut.Count > 0)
                return listaOut.AsQueryable();
            return null;
        }

        public void Update(PromocaoIn In)
        {
            var model = repository.GetAll().Include(x => x.Produto).Where(x => x.Id == In.Id).SingleOrDefault();
            if (model != null)
            {
                if (In.Produto != null)
                {
                    if (model.Produto.Descricao != In.Produto.Descricao) model.Produto.Descricao = In.Produto.Descricao;
                    if (model.Produto.Lote != In.Produto.Lote) model.Produto.Lote = In.Produto.Lote;
                    if (model.Produto.Marca != In.Produto.Marca) model.Produto.Marca = In.Produto.Marca;
                    if (model.Produto.Nome != In.Produto.Nome) model.Produto.Nome = In.Produto.Nome;
                    if (model.Produto.Tipo != In.Produto.Tipo) model.Produto.Tipo = In.Produto.Tipo;
                    if (model.Produto.Validade != In.Produto.Validade) model.Produto.Validade = In.Produto.Validade;
                    if (model.Produto.Valor != In.Produto.Valor) model.Produto.Valor = In.Produto.Valor;
                    if (model.Produto.Codigo != In.Produto.Codigo) model.Produto.Codigo = In.Produto.Codigo;
                    if (model.Produto.DataFabricacao != In.Produto.DataFabricacao) model.Produto.DataFabricacao = In.Produto.DataFabricacao;
                    model.Produto.DataModificacao = DateTimeOffset.UtcNow;
                }
                if (model.DataTermino != In.DataTermino) model.DataTermino = In.DataTermino;
                if (model.Desconto != In.Desconto) model.Desconto = In.Desconto;
                model.DataModificacao = DateTimeOffset.UtcNow;
            }
            repository.Update(model);
        }
    }
}
