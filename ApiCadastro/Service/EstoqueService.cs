﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class EstoqueService : IEstoqueService
    {
        private readonly IEstoqueRepository _repository;
        private readonly IProdutoService _produtoService;
        public EstoqueService(IEstoqueRepository repository, IProdutoService produtoService)
        {
            _repository = repository;
            _produtoService = produtoService;
        }

        public void Add(EstoqueIn In)
        {
            var model = EstoqueExtensions.ToModel(In);
            model.DataCriacao = DateTimeOffset.UtcNow;
            _repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = _repository.GetAll().Where(x => x.Id == Id).SingleOrDefault();
            if (model != null)
            {
                _repository.Remove(model);
                return true;
            }
            return false;
        }

        public Estoque Get(long id)
        {
            var model = _repository.GetAll()
                                    .Include(x => x.Produto).Where(x => x.Id == id).SingleOrDefault();
            if (model != null)
                return model;
            return null;
        }

        public IList<EstoqueOut> GetAll()
        {
            var lista = _repository.GetAll().Include(x => x.Produto).ToList();
            if (lista.Count > 0)
            {
                List<EstoqueOut> ListaOut = new List<EstoqueOut>();
                foreach (var item in lista)
                {
                    var estoque = EstoqueExtensions.ToOut(item);
                    ListaOut.Add(estoque);
                }
                return ListaOut;
            }
            return new List<EstoqueOut>();
        }

        public void Update(EstoqueIn In)
        {
            var model = _repository.GetAll().Include(x => x.Produto).Where(x => x.Id == In.Id).SingleOrDefault();
            if (model != null)
            {
                if (model.QuantidadeTotal != In.QuantidadeTotal) model.QuantidadeTotal = In.QuantidadeTotal;
                if (model.Produto != null)
                {
                    _produtoService.Update(In.Produto);
                    var prod = _produtoService.Get(In.Produto.Id);
                    model.Produto = prod;
                }
            }
            model.DataModificacao = DateTimeOffset.UtcNow;
            _repository.Update(model);
        }
    }
}
