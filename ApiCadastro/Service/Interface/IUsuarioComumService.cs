﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IUsuarioComumService
    {
        //IQueryable<UsuarioComum> TodosUsuariosNormais { get; }
        IQueryable<UsuarioComum> TodosUsuariosNormais();
        void UpdateUsuarioNormal(UsuarioComum usuario);
        void RemoveUsuarioNormal(string userId);
        UsuarioComum GetUsuario(string Id);
    }
}
