﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IContatoService
    {
        IList<ContatoOut> GetAll();
        Contato Get(long id);
        void Add(ContatoIn In);
        void Update(ContatoIn In);
        bool Delete(long Id);
    }
}
