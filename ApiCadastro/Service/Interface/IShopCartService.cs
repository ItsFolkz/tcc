﻿using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IShopCartService
    {
        IQueryable<ShopCartOut> GetAll();
        ShopCartOut Get(long id);
        void Add(ShopCartIn In);
        void Update(ShopCartIn In);
        bool Delete(long Id);
    }
}
