﻿using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IShopItemService
    {
        IQueryable<ShopItemOut> GetAll();
        ShopItemOut Get(long id);
        void Add(ShopItemIn In);
        void Update(ShopItemIn In);
        bool Delete(long Id);
    }
}
