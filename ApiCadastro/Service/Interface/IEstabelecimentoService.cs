﻿using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IEstabelecimentoService
    {
        IQueryable<EstabelecimentoOut> GetAll();
        EstabelecimentoOut Get(long id);
        void Add(EstabelecimentoIn In);
        void Update(EstabelecimentoIn In);
        bool Delete(long Id);
    }
}
