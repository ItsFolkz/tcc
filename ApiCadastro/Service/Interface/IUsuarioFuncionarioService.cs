﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IUsuarioFuncionarioService
    {
        IQueryable<UsuarioFuncionario> TodosFuncionarios { get; }
        void UpdateFuncionario(UsuarioFuncionario usuario);
        void RemoveFuncionario(string userId);
        UsuarioFuncionario GetUsuario(string Id);
    }
}
