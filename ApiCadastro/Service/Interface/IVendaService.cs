﻿using Infra.Models.Middleware;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IVendaService
    {
        IQueryable<VendaOut> GetAll();
        VendaOut Get(long id);
        void Add(VendaIn In);
        void Update(VendaIn In);
        bool Delete(long Id);
        MiddlewareResponse EfetuarCompra(VendaIn In);
    }
}
