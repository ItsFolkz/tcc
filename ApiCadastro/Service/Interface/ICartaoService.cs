﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface ICartaoService
    {
        IQueryable<CartaoOut> GetAll();
        CartaoOut Get(long id);
        void Add(CartaoIn In);
        void Update(CartaoIn In);
        bool Delete(long Id);
    }
}
