﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IProdutoService
    {
        IQueryable<ProdutoOut> TodosProdutos();
        Produto Get(long id);
        void Add(ProdutoIn In);
        void Update(ProdutoIn In);
        bool Delete(long Id);
    }
}
