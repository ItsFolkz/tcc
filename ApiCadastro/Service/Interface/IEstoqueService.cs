﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IEstoqueService
    {
        IList<EstoqueOut> GetAll();
        Estoque Get(long id);
        void Add(EstoqueIn In);
        void Update(EstoqueIn In);
        bool Delete(long Id);
    }
}
