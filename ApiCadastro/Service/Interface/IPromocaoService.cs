﻿using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IPromocaoService
    {
        IQueryable<PromocaoOut> GetAll();
        PromocaoOut Get(long id);
        void Add(PromocaoIn In);
        void Update(PromocaoIn In);
        bool Delete(long Id);
    }
}
