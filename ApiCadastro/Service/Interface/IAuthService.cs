﻿using Infra.Models.Database;
using Infra.Models.Enums;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IAuthService
    {
        Task<AuthOut> Authenticate(LoginIn login);
        Task<bool> Register(RegisterIn register);
        AuthOut RefreshToken(string rfToken);
    }
}
