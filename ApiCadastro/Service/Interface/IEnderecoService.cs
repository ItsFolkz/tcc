﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service.Interface
{
    public interface IEnderecoService
    {
        IList<EnderecoOut> GetAll();
        Endereco Get(long id);
        void Add(EnderecoIn In);
        void Update(EnderecoIn In);
        bool Delete(long Id);
    }
}
