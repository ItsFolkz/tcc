﻿using ApiCadastro.Repository.Interface;
using ApiCadastro.Service.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCadastro.Service
{
    public class EnderecoService : IEnderecoService
    {
        private readonly IEnderecoRepository _repository;
        public EnderecoService(IEnderecoRepository repository)
        {
            _repository = repository;
        }

        public void Add(EnderecoIn In)
        {
            var model = EnderecoExtensions.ToModel(In);
            _repository.Add(model);
        }

        public bool Delete(long Id)
        {
            var model = _repository.Get().Where(x => x.Id == Id).SingleOrDefault();
            if (model == null)
                return false;

            _repository.Remove(model);
            return true;
        }

        public Endereco Get(long id)
        {
            var model = _repository.Get().Where(x => x.Id == id).SingleOrDefault();
            if (model == null)
                return null;
            return model;
        }

        public IList<EnderecoOut> GetAll()
        {
            var listaEndereco = _repository.Get().ToList();
            List<EnderecoOut> Enderecos = new List<EnderecoOut>();
            foreach (var item in listaEndereco)
            {
                var endereco = EnderecoExtensions.ToOut(item);
                Enderecos.Add(endereco);
            }

            if (Enderecos.Count > 0)
                return Enderecos;

            return null;
        }

        public void Update(EnderecoIn In)
        {
            var model = _repository.Get().Where(x => x.Id == In.Id).SingleOrDefault();

            if (model != null)
            {
                if (model.Logradouro != In.Logradouro) model.Logradouro = In.Logradouro;
                if (model.Numero != In.Numero) model.Numero = In.Numero;
                if (model.Estado != In.Estado) model.Estado = In.Estado;
                if (model.Complemento != In.Complemento) model.Complemento = In.Complemento;
                if (model.Cidade != In.Cidade) model.Cidade = In.Cidade;
                if (model.Cep != In.Cep) model.Cep = In.Cep;
                if (model.Bairro != In.Bairro) model.Bairro = In.Bairro;

                _repository.Update(model);
            }
        }
    }
}
