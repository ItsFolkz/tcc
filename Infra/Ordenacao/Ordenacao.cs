﻿using Infra.Models.Database;
using Infra.Ordenacao.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Infra.Ordenacao
{
    public static class Ordenacao
    {
        public static IQueryable<UsuarioFuncionario> AplicaOrdenacaoFuncionario(this IQueryable<UsuarioFuncionario> query, Ordem ordem)
        {
            if (!String.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }
        public static IQueryable<UsuarioComum> AplicaOrdenacaoUsuarioNormal(this IQueryable<UsuarioComum> query, Ordem ordem)
        {
            if (!String.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }
    }
}
