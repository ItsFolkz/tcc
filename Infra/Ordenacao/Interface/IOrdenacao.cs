﻿using Infra.Ordenacao.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Ordenacao.Interface
{
    public interface IOrdenacao<T> where T : class
    {
        IQueryable<T> AplicaOrdenacao(IQueryable<T> query, Ordem ordem);
    }
}
