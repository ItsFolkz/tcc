﻿using Infra.Ordenacao.Interface;
using Infra.Ordenacao.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Infra.Ordenacao
{
    public class Order<T> : IOrdenacao<T> where T : class
    {
        public IQueryable<T> AplicaOrdenacao(IQueryable<T> query, Ordem ordem)
        {
            if (!String.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }
    }
}
