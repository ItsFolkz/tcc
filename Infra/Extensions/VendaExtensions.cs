﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class VendaExtensions
    {
        public static Venda ToModel(VendaIn In)
        {
            Venda venda = new Venda()
            {
                Id = In.Id,
                DataVenda = In.DataVenda,
                ShopCart = In.ShopCart != null ? ShopCartExtensions.ToModel(In.ShopCart) : null,
                ShopCartId = In.ShopCartId,
                CartaoId = In.CartaoId,
                Cartao = In.Cartao != null ? CartaoExtensions.ToModel(In.Cartao) : null,
                Status = In.Status,
                Usuario = In.Usuario,
                UsuarioId = In.UsuarioId
            };
            return venda;
        }

        public static VendaOut ToOut(Venda model)
        {
            VendaOut Out = new VendaOut()
            {
                Id = model.Id,
                DataVenda = model.DataVenda,
                ShopCart = model.ShopCart != null ? ShopCartExtensions.ToOut(model.ShopCart) : null,
                ShopCartId = model.ShopCartId,
                CartaoId = model.CartaoId,
                Cartao = model.Cartao != null ? CartaoExtensions.ToOut(model.Cartao) : null,
                Status = model.Status,
                Usuario = model.Usuario,
                UsuarioId = model.UsuarioId
            };
            return Out;
        }
    }
}
