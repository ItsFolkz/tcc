﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class ContatoExtensions
    {
        public static Contato ToModel(ContatoIn In)
        {
            Contato contato = new Contato
            {
                Id = In.Id,
                Cargo = In.Cargo,
                Celular = In.Celular,
                NomeCompleto = In.NomeCompleto,
                Telefone = In.Telefone,
                Endereco = In.Endereco != null ? EnderecoExtensions.ToModel(In.Endereco) : null
            };

            ModeloBaseExtensions.ToModel(In, contato);
            return contato;
        }

        public static ContatoOut ToOut(Contato contato)
        {
            ContatoOut Out = new ContatoOut
            {
                Id = contato.Id,
                Cargo = contato.Cargo,
                Celular = contato.Celular,
                NomeCompleto = contato.NomeCompleto,
                Telefone = contato.Telefone,
                Endereco = contato.Endereco != null ? EnderecoExtensions.ToOut(contato.Endereco) : null
            };

            ModeloBaseExtensions.ToOut(contato, Out);
            return Out;
        }
    }
}
