﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class ModeloBaseExtensions
    {
        public static void ToModel(this ModeloBaseIn In, ModeloBase modeloBase)
        {
            modeloBase.Id = In.Id;
            modeloBase.ResponsavelCriacao = In.ResponsavelCriacao;
            modeloBase.ResponsavelModificacao = In.ResponsavelModificacao;
            modeloBase.DataCriacao = In.DataCriacao;
            modeloBase.DataModificacao = In.DataModificacao;
        }

        public static void ToOut(this ModeloBase modeloBase, ModeloBaseOut Out)
        {
            Out.Id = modeloBase.Id;
            Out.ResponsavelCriacao = modeloBase.ResponsavelCriacao;
            Out.ResponsavelModificacao = modeloBase.ResponsavelModificacao;
            Out.DataCriacao = modeloBase.DataCriacao;
            Out.DataModificacao = modeloBase.DataModificacao;
        }
    }
}
