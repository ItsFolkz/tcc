﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class CartaoExtensions
    {
        public static Cartao ToModel(CartaoIn In)
        {
            Cartao cartao = new Cartao()
            {
                CodigoSeguranca = In.CodigoSeguranca,
                CpfTitular = In.CpfTitular,
                DataValidade = In.DataValidade,
                Id = In.Id,
                NomeTitular = In.NomeTitular,
                NumeroCartao = In.NumeroCartao,
                Ultimos4Digitos = In.Ultimos4Digitos
            };
            return cartao;
        }

        public static CartaoOut ToOut(Cartao model)
        {
            CartaoOut Out = new CartaoOut()
            {
                CodigoSeguranca = model.CodigoSeguranca,
                CpfTitular = model.CpfTitular,
                DataValidade = model.DataValidade,
                Id = model.Id,
                NomeTitular = model.NomeTitular,
                NumeroCartao = model.NumeroCartao,
                Ultimos4Digitos = model.Ultimos4Digitos
            };
            return Out;
        }
    }
}
