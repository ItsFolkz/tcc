﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class ProdutoExtensions
    {
        public static Produto ToModel(this ProdutoIn In)
        {
            Produto produto = new Produto
            {
                Descricao = In.Descricao,
                Lote = In.Lote,
                Marca = In.Marca,
                Nome = In.Nome,
                Tipo = In.Tipo,
                Validade = In.Validade,
                Valor = In.Valor,
                DataFabricacao = In.DataFabricacao,
                Codigo = In.Codigo
            };

            ModeloBaseExtensions.ToModel(In, produto);

            return produto;
        }

        public static ProdutoOut ToOut(this Produto produto)
        {
            ProdutoOut Out = new ProdutoOut
            {
                Descricao = produto.Descricao,
                Lote = produto.Lote,
                Marca = produto.Marca,
                Nome = produto.Nome,
                Tipo = produto.Tipo,
                Validade = produto.Validade,
                Valor = produto.Valor,
                DataFabricacao = produto.DataFabricacao,
                Codigo = produto.Codigo
            };

            ModeloBaseExtensions.ToOut(produto, Out);

            return Out;
        }
    }
}
