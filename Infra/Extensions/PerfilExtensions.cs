﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class PerfilExtensions
    {
        public static Perfil ToModel(PerfilIn In)
        {
            Perfil perfil = new Perfil()
            {
                Id = In.Id,
                Descricao = In.Descricao,
                TipoPerfil = In.TipoPerfil
            };
            return perfil;
        }

        public static PerfilOut ToOut(Perfil model)
        {
            PerfilOut Out = new PerfilOut()
            {
                Id = model.Id,
                Descricao = model.Descricao,
                TipoPerfil = model.TipoPerfil
            };
            return Out;
        }
    }
}
