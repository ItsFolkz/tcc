﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class PromocaoExtensions
    {
        public static Promocao ToModel(PromocaoIn In)
        {
            Promocao promocao = new Promocao()
            {
                Produto = In.Produto != null ? ProdutoExtensions.ToModel(In.Produto) : null,
                ProdutoId = In.ProdutoId,
                Desconto = In.Desconto,
                DataTermino = In.DataTermino
            };
            ModeloBaseExtensions.ToModel(In, promocao);

            return promocao;
        }

        public static PromocaoOut ToOut(Promocao model)
        {
            PromocaoOut Out = new PromocaoOut()
            {
                Produto = model.Produto != null ? ProdutoExtensions.ToOut(model.Produto) : null,
                ProdutoId = model.ProdutoId,
                Desconto = model.Desconto,
                DataTermino = model.DataTermino
            };
            ModeloBaseExtensions.ToOut(model, Out);

            return Out;
        }
    }
}
