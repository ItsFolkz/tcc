﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class ShopCartExtensions
    {
        public static ShopCart ToModel(ShopCartIn In)
        {
            ShopCart cart = new ShopCart()
            {
                Id = In.Id,
                ValorTotal = In.ValorTotal
            };
            foreach (var item in In.Items)
            {
                cart.Items.Add(ShopItemExtensions.ToModel(item));
            }

            return cart;
        }

        public static ShopCartOut ToOut(ShopCart model)
        {
            ShopCartOut Out = new ShopCartOut()
            {
                Id = model.Id,
                ValorTotal = model.ValorTotal
            };
            foreach (var item in model.Items)
            {
                Out.Items.Add(ShopItemExtensions.ToOut(item));
            }

            return Out;
        }
    }
}
