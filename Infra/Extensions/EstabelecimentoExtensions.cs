﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class EstabelecimentoExtensions
    {
        public static Estabelecimento ToModel(EstabelecimentoIn In)
        {
            Estabelecimento estabelecimento = new Estabelecimento()
            {
                EnderecoId = In.EnderecoId,
                Endereco = In.Endereco != null ? EnderecoExtensions.ToModel(In.Endereco) : null,
                Nome = In.Nome,
                RazaoSocial = In.RazaoSocial,
                Responsavel = In.Responsavel != null ? ContatoExtensions.ToModel(In.Responsavel) : null,
                ResponsavelId = In.ResponsavelId
            };
            ModeloBaseExtensions.ToModel(In, estabelecimento);

            return estabelecimento;
        }

        public static EstabelecimentoOut ToOut(Estabelecimento model)
        {
            EstabelecimentoOut Out = new EstabelecimentoOut()
            {
                EnderecoId = model.EnderecoId,
                Endereco = model.Endereco != null ? EnderecoExtensions.ToOut(model.Endereco) : null,
                Nome = model.Nome,
                RazaoSocial = model.RazaoSocial,
                Responsavel = model.Responsavel != null ? ContatoExtensions.ToOut(model.Responsavel) : null,
                ResponsavelId = model.ResponsavelId
            };
            ModeloBaseExtensions.ToOut(model, Out);

            return Out;
        }
    }
}
