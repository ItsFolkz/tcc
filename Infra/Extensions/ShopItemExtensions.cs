﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class ShopItemExtensions
    {
        public static ShopItem ToModel(ShopItemIn In)
        {
            ShopItem shop = new ShopItem()
            {
                Id = In.Id,
                ProdutoId = In.ProdutoId,
                Produto = In.Produto != null ? ProdutoExtensions.ToModel(In.Produto) : null,
                Quantidade = In.Quantidade,
                ValorTotal = In.ValorTotal
            };
            return shop;
        }

        public static ShopItemOut ToOut(ShopItem model)
        {
            ShopItemOut Out = new ShopItemOut()
            {
                Id = model.Id,
                ProdutoId = model.ProdutoId,
                Produto = model.Produto != null ? ProdutoExtensions.ToOut(model.Produto) : null,
                Quantidade = model.Quantidade,
                ValorTotal = model.ValorTotal
            };
            return Out;
        }
    }
}
