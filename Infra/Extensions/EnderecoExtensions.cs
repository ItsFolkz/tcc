﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class EnderecoExtensions
    {
        public static Endereco ToModel(this EnderecoIn In)
        {
            Endereco endereco = new Endereco
            {
                Bairro = In.Bairro,
                Cep = In.Cep,
                Cidade = In.Cidade,
                Complemento = In.Complemento,
                Estado = In.Estado,
                Logradouro = In.Logradouro,
                Numero = In.Numero
            };

            ModeloBaseExtensions.ToModel(In, endereco);
            return endereco;
        }

        public static EnderecoOut ToOut(this Endereco endereco)
        {
            EnderecoOut Out = new EnderecoOut
            {
                Bairro = endereco.Bairro,
                Cep = endereco.Cep,
                Cidade = endereco.Cidade,
                Complemento = endereco.Complemento,
                Estado = endereco.Estado,
                Logradouro = endereco.Logradouro,
                Numero = endereco.Numero
            };

            ModeloBaseExtensions.ToOut(endereco, Out);
            return Out;
        }
    }
}
