﻿using Infra.Models.Database;
using Infra.Models.ViewModel.In;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Extensions
{
    public static class EstoqueExtensions
    {
        public static Estoque ToModel(EstoqueIn In)
        {
            Estoque estoque = new Estoque
            {
                Produto = In.Produto != null ? ProdutoExtensions.ToModel(In.Produto) : null,
                QuantidadeTotal = In.QuantidadeTotal,
            };

            ModeloBaseExtensions.ToModel(In, estoque);
            return estoque;
        }

        public static EstoqueOut ToOut(Estoque estoque)
        {

            EstoqueOut Out = new EstoqueOut
            {
                Produto = estoque.Produto != null ? ProdutoExtensions.ToOut(estoque.Produto) : null,
                QuantidadeTotal = estoque.QuantidadeTotal,
            };

            ModeloBaseExtensions.ToOut(estoque, Out);
            return Out;
        }
    }
}
