﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Filtros
{
    public class UsuarioComumFiltros
    {
        public string NomeCompleto { get; set; }
        public string DocumentoIdentificacao { get; set; }
    }
    public static class FiltroUsuarioNormal
    {
        public static IQueryable<UsuarioComum> AplicaFiltroUsuarioNormal(this IQueryable<UsuarioComum> query, UsuarioComumFiltros filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.NomeCompleto))
                    query = query.Where(x => x.NomeCompleto.Contains(filtro.NomeCompleto));
                if (!string.IsNullOrWhiteSpace(filtro.DocumentoIdentificacao))
                    query = query.Where(x => x.DocumentoIdentificacao.Equals(filtro.DocumentoIdentificacao));
            }
            return query;
        }
    }
}
