﻿using Infra.Models.Database;
using Infra.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Filtros
{
    public class ProdutoFiltros
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Marca { get; set; }
        public string DataValidade { get; set; }
        public string DataInicio { get; set; }
        public string DataTermino { get; set; }
    }

    public static class FiltroProduto
    {
        public static IQueryable<ProdutoOut> AplicaFiltroProduto(this IQueryable<ProdutoOut> query, ProdutoFiltros filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.DataInicio) && !string.IsNullOrWhiteSpace(filtro.DataTermino))
                {
                    DateTime startDate = Convert.ToDateTime(filtro.DataInicio);
                    DateTime endDate = Convert.ToDateTime(filtro.DataTermino);
                    query = query.Where(x => x.DataCriacao >= startDate && x.DataCriacao <= endDate);
                }
                if (!string.IsNullOrWhiteSpace(filtro.DataValidade))
                {
                    DateTime validade = Convert.ToDateTime(filtro.DataValidade);
                    query = query.Where(x => x.Validade >= validade);
                }
                if (!string.IsNullOrWhiteSpace(filtro.Codigo))
                {
                    query = query.Where(x => x.Codigo.Equals(filtro.Codigo));
                }
                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Nome.Contains(filtro.Nome));
                if (!string.IsNullOrWhiteSpace(filtro.Marca))
                    query = query.Where(x => x.Marca.Contains(filtro.Marca));
            }
            return query;
        }
    }
}
