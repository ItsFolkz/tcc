﻿using Infra.Models.Enums;
using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Filtros
{
    public class VendaFiltros
    {
        public string DataDaVenda { get; set; }
        public string NomeDoCliente { get; set; }
        public string IdCliente { get; set; }
        public StatusEnum[] StatusVenda { get; set; }
    }

    public static class FiltroVenda
    {
        public static IQueryable<VendaOut> AplicaFiltroVenda(this IQueryable<VendaOut> query, VendaFiltros filtro)
        {
            query = query.Include(x => x.ShopCart)
                            .ThenInclude(x => x.Items)
                                .ThenInclude(x => x.Produto)
                         .Include(e => e.Usuario);
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.DataDaVenda))
                {
                    DateTime dataVenda = Convert.ToDateTime(filtro.DataDaVenda);
                    query = query.Where(x => x.DataVenda == dataVenda);
                }
                if (!string.IsNullOrWhiteSpace(filtro.NomeDoCliente))
                {
                    query = query.Where(x => x.Usuario.NomeCompleto.Contains(filtro.NomeDoCliente));
                }
                if (filtro.StatusVenda != null && filtro.StatusVenda.Length > 0)
                {
                    query = query.Where(x => filtro.StatusVenda.Contains(x.Status));
                }
                if (!string.IsNullOrWhiteSpace(filtro.IdCliente))
                {
                    query = query.Where(x => x.Usuario.Id.Equals(filtro.IdCliente));
                }
            }
            return query;
        }
    }
}
