﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Filtros
{
    public class UsuarioFuncionarioFiltros
    {
        public string IdFuncionario { get; set; }
        public string Cargo { get; set; }
        public string DocumentoIdentificacao { get; set; }
    }
    public static class FiltroFuncionario 
    {
        public static IQueryable<UsuarioFuncionario> AplicaFiltroFuncionario(this IQueryable<UsuarioFuncionario> query, UsuarioFuncionarioFiltros filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Cargo))
                    query = query.Where(x => x.Cargo.Equals(filtro.Cargo));
                if (!string.IsNullOrWhiteSpace(filtro.IdFuncionario))
                    query = query.Where(x => x.IdFuncionario.Equals(filtro.IdFuncionario));
                if (!string.IsNullOrWhiteSpace(filtro.DocumentoIdentificacao))
                    query = query.Where(x => x.DocumentoIdentificacao.Equals(filtro.DocumentoIdentificacao));
            }
            return query;
        }
    }
}
