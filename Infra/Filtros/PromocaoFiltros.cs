﻿using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Filtros
{
    public class PromocaoFiltros
    {
        public string NomeProduto { get; set; }
        public string PromocaoExpirada { get; set; } // Bool
        public string PromocaoValida { get; set; } // Bool
    }

    public static class FiltroPromocao
    {
        public static IQueryable<PromocaoOut> AplicaFiltroPromocao(this IQueryable<PromocaoOut> query, PromocaoFiltros filtro)
        {
            query = query.Include(x => x.Produto);
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.PromocaoExpirada))
                {
                    bool isPromoExpirada = Convert.ToBoolean(filtro.PromocaoExpirada);
                    DateTimeOffset dataAtual = DateTimeOffset.UtcNow; // 03/06/2020
                    // datatermino = 04/06/2020
                    query = query.Where(x => x.DataTermino < dataAtual);
                }
                if (!string.IsNullOrWhiteSpace(filtro.PromocaoValida))
                {
                    bool isPromoValida = Convert.ToBoolean(filtro.PromocaoValida);
                    DateTimeOffset dataAtual = DateTimeOffset.UtcNow; // 03/06/2020
                    query = query.Where(x => x.DataTermino > dataAtual);
                }
                if (!string.IsNullOrWhiteSpace(filtro.NomeProduto))
                {
                    query = query.Where(x => x.Produto.Nome.Contains(filtro.NomeProduto));
                }
            }
            return query;
        }
    }
}
