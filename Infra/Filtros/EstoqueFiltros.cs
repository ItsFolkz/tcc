﻿using Infra.Models.ViewModel.Out;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Filtros
{
    public class EstoqueFiltros
    {
        public long IdProduto { get; set; }
        public string NomeProduto { get; set; }
    }

    public static class FiltroEstoque 
    {
        public static IQueryable<EstoqueOut> AplicaFiltroEstoque(this IQueryable<EstoqueOut> query, EstoqueFiltros filtro)
        {
            query = query.Include(x => x.Produto);
            if (filtro != null)
            {
                if (filtro.IdProduto > 0)
                {
                    query = query.Where(x => x.Produto.Id == filtro.IdProduto);
                }
                if (!string.IsNullOrWhiteSpace(filtro.NomeProduto))
                {
                    query = query.Where(x => x.Produto.Nome.Contains(filtro.NomeProduto));
                }
            }
            return query;
        }
    }
}
