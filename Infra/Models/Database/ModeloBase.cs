﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class ModeloBase
    {
        [Key]
        public long Id { get; set; }
        public string ResponsavelCriacao { get; set; }
        public string ResponsavelModificacao { get; set; }
        public DateTimeOffset DataCriacao { get; set; }
        public DateTimeOffset DataModificacao { get; set; }
    }
}
