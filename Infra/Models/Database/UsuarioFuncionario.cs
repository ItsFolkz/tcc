﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class UsuarioFuncionario : UsuarioBase
    {
        public string Cargo { get; set; }
        public string IdFuncionario { get; set; }
        public long EstabelecimentoId { get; set; }
        public virtual Estabelecimento Estabelecimento { get; set; }
    }
}
