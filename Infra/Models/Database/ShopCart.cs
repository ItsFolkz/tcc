﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class ShopCart
    {
        [Key]
        public long Id { get; set; }
        public List<ShopItem> Items { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
