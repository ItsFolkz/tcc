﻿using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class Perfil
    {
        [Key]
        public long Id { get; set; }
        public string Descricao { get; set; }
        public TipoPerfilEnum TipoPerfil { get; set; }
    }
}
