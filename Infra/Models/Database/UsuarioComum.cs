﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class UsuarioComum : UsuarioBase
    {
        //public long CartaoId { get; set; }
        public virtual List<Cartao> Cartoes { get; set; }
        public long EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
    }
}
