﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Promocao : ModeloBase
    {
        public long ProdutoId { get; set; }
        public Produto Produto { get; set; }
        public DateTimeOffset DataTermino { get; set; }
        public double Desconto { get; set; }
    }
}
