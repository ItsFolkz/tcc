﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class Cartao
    {
        [Key]
        public long Id { get; set; }
        public string NumeroCartao { get; set; }
        public string Ultimos4Digitos { get; set; }
        public string DataValidade { get; set; }
        public string CodigoSeguranca { get; set; }
        public string NomeTitular { get; set; }
        public string CpfTitular { get; set; }
    }
}
