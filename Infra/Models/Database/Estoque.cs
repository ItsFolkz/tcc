﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Estoque : ModeloBase
    {
        public long ProdutoId { get; set; }
        public Produto Produto { get; set; }
        public long QuantidadeTotal { get; set; }
    }
}
