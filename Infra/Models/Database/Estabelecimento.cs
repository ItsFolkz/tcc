﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Estabelecimento : ModeloBase
    {
        public string DocumentoIdentificacao { get; set; }
        public long EnderecoId { get; set; }
        public Endereco Endereco { get; set; }
        public long ResponsavelId { get; set; }
        public Contato Responsavel { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
    }
}
