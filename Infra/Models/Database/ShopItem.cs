﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class ShopItem
    {
        [Key]
        public long Id { get; set; }
        public long ProdutoId { get; set; }
        public Produto Produto { get; set; }
        public long Quantidade { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
