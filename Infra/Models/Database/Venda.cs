﻿using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class Venda
    {
        [Key]
        public long Id { get; set; }
        public DateTimeOffset DataVenda { get; set; }
        public long ShopCartId { get; set; }
        public ShopCart ShopCart { get; set; }
        public long CartaoId { get; set; }
        public Cartao Cartao { get; set; }
        public long UsuarioId { get; set; }
        public UsuarioComum Usuario { get; set; }
        public StatusEnum Status { get; set; }
    }
}
