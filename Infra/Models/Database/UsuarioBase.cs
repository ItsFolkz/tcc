﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public abstract class UsuarioBase : IdentityUser
    {
        public string NomeCompleto { get; set; }
        public string DocumentoIdentificacao { get; set; }
        public string Telefone { get; set; }
        public long PerfilId { get; set; }
        public virtual Perfil Perfil { get; set; }
    }
}
