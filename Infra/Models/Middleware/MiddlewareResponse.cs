﻿using Infra.Models.ViewModel.In;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Middleware
{
    public class MiddlewareResponse
    {
        public VendaIn Compra { get; set; }
        public string RespostaAutorizacao { get; set; }
        public bool Autorizado { get; set; }
    }
}
