﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Enums
{
    public enum TipoPerfilEnum
    {
        Administrador = 1,
        Gerente = 2,
        Estoquista = 3,
        Cliente = 4
    }
}
