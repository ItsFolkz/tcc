﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Enums
{
    public enum StatusEnum
    {
        Cancelada = 0,
        Aguardando = 1,
        Processando = 2,
        Processado = 3,
        Preparando_Entrega = 4,
        Saiu_Para_Entrega = 5,
        Entregue = 6,
    }
}
