﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Enums
{
    public enum TipoLoginEnum
    {
        Admin = 0,
        Normal = 1,
        Funcionario = 2,
    }
}
