﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Enums
{
    public enum TipoEnum
    {
        Bebida_Alcolica = 1,
        Bebidas_Normais = 2,
        Padaria = 3,
        Hortalicas = 4,
        Frios = 5,
        Acougue = 6,
    }
}
