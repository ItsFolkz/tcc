﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class PromocaoIn: ModeloBaseIn
    {
        public long ProdutoId { get; set; }
        public ProdutoIn Produto { get; set; }
        public DateTimeOffset DataTermino { get; set; }
        public double Desconto { get; set; }
    }
}
