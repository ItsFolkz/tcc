﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class ContatoIn : ModeloBaseIn
    {
        public string NomeCompleto { get; set; }
        public long EnderecoId { get; set; }
        public EnderecoIn Endereco { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Cargo { get; set; }
    }
}
