﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class UsuarioFuncionarioIn
    {
        public string Username { get; set; }
        public string NomeCompleto { get; set; }
        public string Telefone { get; set; }
        public string DocumentoIdentificacao { get; set; }
        public string IdFuncionario { get; set; }
        public string Senha { get; set; }
    }
}
