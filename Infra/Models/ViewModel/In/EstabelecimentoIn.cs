﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class EstabelecimentoIn : ModeloBaseIn
    {
        public string DocumentoIdentificacao { get; set; }
        public long EnderecoId { get; set; }
        public EnderecoIn Endereco { get; set; }
        public long ResponsavelId { get; set; }
        public ContatoIn Responsavel { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
    }
}
