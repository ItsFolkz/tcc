﻿using Infra.Models.Database;
using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class VendaIn
    {
        public long Id { get; set; }
        public DateTimeOffset DataVenda { get; set; }
        public long ShopCartId { get; set; }
        public ShopCartIn ShopCart { get; set; }
        public long CartaoId { get; set; }
        public CartaoIn Cartao { get; set; }
        public long UsuarioId { get; set; }
        public UsuarioComum Usuario { get; set; }
        public StatusEnum Status { get; set; }
    }
}
