﻿using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class LoginIn
    {
        public string Username { get; set; }
        public string Senha { get; set; }
        public TipoLoginEnum Tipo { get; set; }
    }
}
