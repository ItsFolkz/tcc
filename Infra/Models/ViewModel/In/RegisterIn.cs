﻿using Infra.Models.Database;
using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class RegisterIn
    {
        public UsuarioComumIn Comum { get; set; }
        public UsuarioFuncionarioIn Funcionario { get; set; }
        public TipoLoginEnum Tipo { get; set; }
    }
}
