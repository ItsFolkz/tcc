﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class EstoqueIn : ModeloBaseIn
    {
        public long ProdutoId { get; set; }
        public ProdutoIn Produto { get; set; }
        public long QuantidadeTotal { get; set; }
    }
}
