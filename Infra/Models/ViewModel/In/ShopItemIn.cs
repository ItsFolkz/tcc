﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class ShopItemIn
    {
        public long Id { get; set; }
        public long ProdutoId { get; set; }
        public ProdutoIn Produto { get; set; }
        public long Quantidade { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
