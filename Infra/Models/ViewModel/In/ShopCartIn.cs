﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class ShopCartIn
    {
        public long Id { get; set; }
        public List<ShopItemIn> Items { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
