﻿using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.In
{
    public class ProdutoIn : ModeloBaseIn
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public string Marca { get; set; }
        public DateTimeOffset Validade { get; set; }
        public string Lote { get; set; }
        public DateTimeOffset DataFabricacao { get; set; }
        public TipoEnum Tipo { get; set; }
    }
}
