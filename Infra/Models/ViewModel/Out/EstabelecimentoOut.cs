﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class EstabelecimentoOut : ModeloBaseOut
    {
        public string DocumentoIdentificacao { get; set; }
        public long EnderecoId { get; set; }
        public EnderecoOut Endereco { get; set; }
        public long ResponsavelId { get; set; }
        public ContatoOut Responsavel { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
    }
}
