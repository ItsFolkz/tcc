﻿using Infra.Models.Database;
using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class VendaOut
    {
        public long Id { get; set; }
        public DateTimeOffset DataVenda { get; set; }
        public long ShopCartId { get; set; }
        public ShopCartOut ShopCart { get; set; }
        public long CartaoId { get; set; }
        public CartaoOut Cartao { get; set; }
        public long UsuarioId { get; set; }
        public UsuarioComum Usuario { get; set; }
        public StatusEnum Status { get; set; }
    }
}
