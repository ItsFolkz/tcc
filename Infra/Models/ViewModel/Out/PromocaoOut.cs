﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class PromocaoOut : ModeloBaseOut
    {
        public long ProdutoId { get; set; }
        public ProdutoOut Produto { get; set; }
        public DateTimeOffset DataTermino { get; set; }
        public double Desconto { get; set; }
    }
}
