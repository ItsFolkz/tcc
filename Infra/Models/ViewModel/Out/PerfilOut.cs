﻿using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class PerfilOut
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public TipoPerfilEnum TipoPerfil { get; set; }
    }
}
