﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class ShopItemOut
    {
        public long Id { get; set; }
        public long ProdutoId { get; set; }
        public ProdutoOut Produto { get; set; }
        public long Quantidade { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
