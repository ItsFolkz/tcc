﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class ShopCartOut
    {
        public long Id { get; set; }
        public List<ShopItemOut> Items { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
