﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class EstoqueOut : ModeloBaseOut
    {
        public long ProdutoId { get; set; }
        public ProdutoOut Produto { get; set; }
        public long QuantidadeTotal { get; set; }
    }
}
