﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class ContatoOut : ModeloBaseOut
    {
        public string NomeCompleto { get; set; }
        public long EnderecoId { get; set; }
        public EnderecoOut Endereco { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Cargo { get; set; }
    }
}
