﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.ViewModel.Out
{
    public class ModeloBaseOut
    {

        public long Id { get; set; }
        public string ResponsavelCriacao { get; set; }
        public string ResponsavelModificacao { get; set; }
        public DateTimeOffset DataCriacao { get; set; }
        public DateTimeOffset DataModificacao { get; set; }
    }
}
