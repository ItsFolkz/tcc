﻿using Infra.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.RedisModel
{
    public class RefreshToken
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public TipoLoginEnum Tipo { get; set; }
        public string Refresh { get; set; }
        public DateTime Expiration { get; set; }
    }
}
