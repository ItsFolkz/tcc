﻿using Infra.Models.Middleware;
using Infra.Models.ViewModel.In;
using ModuloMiddleware.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModuloMiddleware.Service
{
    public class MiddlewareService : IMiddlewareService
    {
        public MiddlewareResponse executePayment(VendaIn Compra)
        {
            MiddlewareResponse response = new MiddlewareResponse()
            {
                Compra = Compra,
                Autorizado = true,
                RespostaAutorizacao = "Compra foi autorizada com sucesso."
            };
            return response;
        }
    }
}