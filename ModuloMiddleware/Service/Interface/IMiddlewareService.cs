﻿using Infra.Models.Middleware;
using Infra.Models.ViewModel.In;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModuloMiddleware.Service.Interface
{
    public interface IMiddlewareService
    {
        MiddlewareResponse executePayment(VendaIn Compra);
    }
}
